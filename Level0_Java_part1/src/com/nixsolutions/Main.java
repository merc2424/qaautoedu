package com.nixsolutions;

import com.welcome.Hello;

import java.io.InputStreamReader;
import java.io.BufferedReader;


/**
 * Created by dyakonov on 10.11.2016.
 */
public class Main {

    public static void main(String[] args) throws Exception {

        Hello hello = new Hello();

        BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите имя в консоль");
        hello.setupName(buff.readLine());

        hello.welcome();

        System.out.println("Hello, world!");

        hello.byeBye();
    }

}
