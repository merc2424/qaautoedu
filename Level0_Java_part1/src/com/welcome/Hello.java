package com.welcome;

/**
 * Created by dyakonov on 10.11.2016.
 */
public class Hello {

        private String name = "";

        public void setupName(String name){
            this.name = name;
        }

        public void welcome(){
            System.out.println("Hello, " + name);
        }

        public void byeBye(){
            System.out.println("Bye, " + name);
        }
}
