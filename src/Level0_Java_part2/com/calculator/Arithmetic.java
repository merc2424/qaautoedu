package Level0_Java_part2.com.calculator;


/**
 * Created by ksuha on 13.11.2016.
 */
public class Arithmetic {

    public static double arrayMultiplication(double[] array) {
        double result = 1;
        for (double i : array) result *= i;
        return result;
    }


    public static double power(double x, double y) {
        return Math.pow(x, y);
    }

    public static double division(double x, double y) {
        return x / y;
    }


    public static double root(double x) {
        return Math.sqrt(x);
    }
}
