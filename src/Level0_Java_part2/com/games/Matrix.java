package Level0_Java_part2.com.games;

/**
 * Created by dyakonov on 10.11.2016.
 */
public class Matrix {

    public static int[][] getMatrix(int matrixSize) {
        int[][] array = new int[matrixSize][matrixSize];

        int n = 1;
        for (int i = 0; i < matrixSize; i++) {
            for (int y = 0; y < matrixSize; y++) {
                if (n > 9) n = 1;
                array[i][y] = n++;
            }
        }
        return array;
    }

}
