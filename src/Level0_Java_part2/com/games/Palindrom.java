package Level0_Java_part2.com.games;

/**
 * Created by ksuha on 15.11.2016.
 */
public class Palindrom {

    public static boolean checkWord(String someWord) {
        String reverse = "";
        someWord = someWord.toLowerCase();
        for (int i = someWord.length() - 1; i >= 0; --i) reverse += someWord.charAt(i);
        return someWord.equals(reverse);
    }

    public static boolean checkPhrase(String somePhrase) {
        somePhrase = somePhrase.toLowerCase();
        somePhrase = somePhrase.replace(" ", "");
        return checkWord(somePhrase);
    }
}
