package Level0_Java_part2.com.games;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by dyakonov on 14.11.2016.
 */
public class Snail {

    public static int[][] calculateSnail(int size) throws IOException {
        int[][] snail = new int[size][size];
        int i, j;
        if (checkNumber(size)) i = j = size / 2 - 1;
        else i = j = size / 2;
        int count = 1;
        int steps = 2;

        snail:
        {
            while (true) {
                if (count == 1) {
                    snail[i][j] = count;
                    j++;
                    count++;
                }

                for (int x = i + steps; i < x; i++) {   // Вниз i+
                    snail[i][j] = count++;
                    if (i == x - 1) {
                        j--;
                        if (steps > 2) steps++;
                        break;
                    }
                }

                for (int x = j - steps; j > x; j--) {   // Влево j-
                    snail[i][j] = count++;
                    if (count == size * size + 1) break snail;
                    if (j == x + 1) {
                        i--;
                        break;
                    }
                }

                for (int x = i - steps; i > x; i--) {   // Вверх i-
                    snail[i][j] = count++;
                    if (i == x + 1) {
                        j++;
                        steps++;
                        break;
                    }
                }

                for (int x = j + steps; j < x; j++) {   // Вправо j+
                    snail[i][j] = count++;
                    if (count == size * size + 1) break snail;
                    if (j == x - 1) {
                        i++;
                        break;
                    }
                }
            }
        }
        return snail;
    }

    public static boolean checkNumber(int a) throws IOException {               // Позволяет узнать чётное ли число
        if (a > 0 & a % 2 == 0) return true;
        else return false;
    }
}
