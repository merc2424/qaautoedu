package Level0_Java_part2.com.nixsolutions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


import Level0_Java_part2.com.calculator.Arithmetic;
import Level0_Java_part2.com.games.Matrix;
import Level0_Java_part2.com.games.Palindrom;
import Level0_Java_part2.com.games.Snail;

/**
 * Created by dyakonov on 11.11.2016.
 */
public class Main {
    static BufferedReader buffer1 = new BufferedReader(new InputStreamReader(System.in));


    public static void main(String[] args) throws IOException {

        generalMenu();

    }


    public static void generalMenu() {
        try {
            Thread.sleep(600);
            consoleClener();
            printMenu();

            switch (Integer.parseInt(buffer1.readLine())) {
                case 1:
                    gamesMenu();
                case 2:
                    arithmeticMenu();
                case 0:
                    System.out.println("Всего доброго!");
                    System.exit(1);

                default: {
                    printInputErrorMessage();
                    generalMenu();
                }
            }
        } catch (IOException i) {
            printInputErrorMessage();
            generalMenu();
        } catch (NumberFormatException n) {
            printInputErrorMessage();
            generalMenu();
        } catch (InterruptedException e) {
            generalMenu();
        }

    }


    public static void arithmeticMenu() {
        try {
            Thread.sleep(600);
            consoleClener();
            printArithmeticMenu();

            switch (Integer.parseInt(buffer1.readLine())) {
                case 1:
                    testArrayMultiplication();
                case 2:
                    testPower();
                case 3:
                    testDivision();
                case 4:
                    testRoot();
                case 0:
                    generalMenu();
                default:
                    printInputErrorMessage();
                    arithmeticMenu();
            }
        } catch (IOException i) {
            printInputErrorMessage();
            arithmeticMenu();
        } catch (NumberFormatException n) {
            printInputErrorMessage();
            arithmeticMenu();
        } catch (InterruptedException r) {
            arithmeticMenu();
        }
    }


    public static void gamesMenu() {
        try {
            Thread.sleep(600);
            consoleClener();
            printGamesMenu();
            switch (Integer.parseInt(buffer1.readLine())) {
                case 1:
                    testMatrix();
                case 2:
                    testSnail();
                case 3:
                    testPalindromWord();
                case 4:
                    testPalindromPhrase();
                case 0:
                    generalMenu();
                default:
                    printInputErrorMessage();
                    gamesMenu();
            }
        } catch (IOException i) {
            printInputErrorMessage();
            gamesMenu();
        } catch (NumberFormatException n) {
            printInputErrorMessage();
            gamesMenu();
        } catch (InterruptedException r) {
            gamesMenu();
        }
    }


    public static void testMatrix() throws IOException {
        System.out.println("Для тестирования метода Matrix, введите длинну стороны матрицы в диапазоне 1 - 9");
        System.out.println("Матрица будет заполнена цифрами от одного до девяти, слева направо, сверху вниз");
        int sideLength = Integer.parseInt(buffer1.readLine());
        if (sideLength > 0 & sideLength <= 9) {
            printMatrix(Matrix.getMatrix(sideLength));
        } else {
            printInputErrorMessage();
            testMatrix();

        }
        printReturnToMenuMessage();
        switch (Integer.parseInt(buffer1.readLine())) {
            case 1:
                testMatrix();
            case 0:
                gamesMenu();
            default:
                printInputErrorMessage();
                gamesMenu();
        }


    }


    public static void testArrayMultiplication() throws IOException {
        System.out.println("Этот метот перемножает массив введенных чисел.");
        System.out.println("Введите с клавиатуры пять цифр, после каждой нажимая ввод.");
        double[] array = new double[5];
        for (int i = 0; i < array.length; i++) {
            array[i] = Double.parseDouble(buffer1.readLine());
        }
        System.out.println("Результат: " + Arithmetic.arrayMultiplication(array));
        printReturnToMenuMessage();
        switch (Integer.parseInt(buffer1.readLine())) {
            case 1:
                testArrayMultiplication();
            case 0:
                arithmeticMenu();
            default:
                arithmeticMenu();
        }
    }

    public static void testPower() throws IOException {
        double x, y;
        System.out.println("Этот метот возводит число в заданную степень.");
        System.out.println("Введите число:");
        x = Double.parseDouble(buffer1.readLine());
        System.out.println("Отлично, а теперь введите степень, в которую вы хотите возвести данное число");
        y = Double.parseDouble(buffer1.readLine());
        System.out.println("Результат: " + Arithmetic.power(x, y));
        printReturnToMenuMessage();
        switch (Integer.parseInt(buffer1.readLine())) {
            case 1:
                testPower();
            case 0:
                arithmeticMenu();
            default:
                arithmeticMenu();
        }
    }

    public static void testDivision() throws IOException {
        double x, y;
        try {
            System.out.println("Метод division производит деление одного вводимого числа на другое.");
            System.out.println("Введите делимое:");
            x = Double.parseDouble(buffer1.readLine());
            System.out.println("Теперь введите делитель:");
            y = Double.parseDouble(buffer1.readLine());
            if (y == 0) {
                System.out.println("А-я-яй. На ноль делить нельзя)");
                testDivision();
            }
            System.out.println("Частное равняется: " + Arithmetic.division(x, y));
            printReturnToMenuMessage();
            switch (Integer.parseInt(buffer1.readLine())) {
                case 1:
                    testDivision();
                case 0:
                    arithmeticMenu();
                default:
                    arithmeticMenu();
            }
        } catch (IOException i) {
            printInputErrorMessage();
            arithmeticMenu();
        } catch (NumberFormatException n) {
            printInputErrorMessage();
            arithmeticMenu();
        }


    }


    public static void testRoot() throws IOException {
        double x;
        System.out.println("Метод root извлекает квадратный корень из введенного числа.");
        System.out.println("Введите число:");
        x = Double.parseDouble(buffer1.readLine());
        System.out.println("Результат вычисления = " + Arithmetic.root(x));
        printReturnToMenuMessage();
        switch (Integer.parseInt(buffer1.readLine())) {
            case 1:
                testRoot();
            case 0:
                arithmeticMenu();
            default:
                printInputErrorMessage();
                arithmeticMenu();
        }

    }


    public static void testSnail() throws IOException {
        System.out.println("Для тестирования метода Snail, введите длинну стороны матрицы, начиная от 4 и больше");
        int sideLength = Integer.parseInt(buffer1.readLine());
        if (sideLength < 4) {
            System.out.println("Некорретный ввод! Попробуйте ещё раз.");
            testSnail();
        }

        if (sideLength > 31) {
            System.out.println("Многовато, давай чуть меньше =)");
            testSnail();
        }
        printMatrix(Snail.calculateSnail(sideLength));
        printReturnToMenuMessage();
        switch (Integer.parseInt(buffer1.readLine())) {
            case 1:
                testSnail();
            case 0:
                gamesMenu();
            default:
                printInputErrorMessage();
                gamesMenu();
        }
    }

    public static void testPalindromWord() throws IOException {
        System.out.println("Введите слово, что бы узнать полиндром ли оно");
        String word = buffer1.readLine();
        if (word.equals("")) {
            printInputErrorMessage();
            testPalindromPhrase();
        }
        if (Palindrom.checkWord(word)) {
            System.out.println("Палиндром!");
        } else System.out.println("Не палиндром!");
        printReturnToMenuMessage();
        switch (Integer.parseInt(buffer1.readLine())) {
            case 1:
                testPalindromWord();
            case 0:
                gamesMenu();
            default:
                printInputErrorMessage();
                gamesMenu();
        }
    }

    public static void testPalindromPhrase() throws IOException {
        System.out.println("Введите фразу, что-бы узнать палндром ли она");
        String phrase = buffer1.readLine();
        if (phrase.equals("")) {
            printInputErrorMessage();
            testPalindromPhrase();
        }
        if (Palindrom.checkPhrase(phrase)) {
            System.out.println("Палиндром!");
        } else System.out.println("Не палиндром!");
        printReturnToMenuMessage();
        switch (Integer.parseInt(buffer1.readLine())) {
            case 1:
                testPalindromPhrase();
            case 0:
                gamesMenu();
            default:
                printInputErrorMessage();
                gamesMenu();
        }


    }


    public static void printMenu() {
        System.out.println("==========-Главное Меню-==========");
        System.out.println("Для навигации по меню используйте ввод цифр с клавиатуры:");
        System.out.println("1 - Тестирование пакета games");
        System.out.println("2 - Тестирование пакета calculator");
        System.out.println("0 - Завершить работу программы");
        System.out.println("==================================");

    }

    public static void printArithmeticMenu() {
        System.out.println("===========-calculator-===========");
        System.out.println("Для навигации по меню используйте ввод цифр с клавиатуры:");
        System.out.println("1 - Тестирование метода arrayMultiplication класса Arithmetic");
        System.out.println("2 - Тестирование метода power класса Arithmetic");
        System.out.println("3 - Тестирование метода division класса Arithmetic");
        System.out.println("4 - Тестирование метода root класса Arithmetic");
        System.out.println("0 - Вернуться в основное меню");
        System.out.println("==================================");
    }

    public static void printGamesMenu() {
        System.out.println("=============-games-==============");
        System.out.println("Для навигации по меню используйте ввод цифр с клавиатуры:");
        System.out.println("1 - Тестирование метода getMatrix класса Matrix");
        System.out.println("2 - Тестирование метода calculateSnail класса Snail");
        System.out.println("3 - Тестирование метода palindromWord класса Palindrom");
        System.out.println("4 - Тестирование метода palindromPhrase класса Palindrom");
        System.out.println("0 - Вернуться в основное меню");
        System.out.println("==================================");
    }


    public static void printMatrix(int[][] array) {
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                System.out.print(array[i][j]);
                if (array[i][j] < 10) System.out.print("   ");
                if (array[i][j] >= 10 & array[i][j] <= 99) System.out.print("  ");
                if (array[i][j] >= 100) System.out.print(" ");
                if (j == array.length - 1) System.out.println();
            }
        }
        System.out.println();
    }


    public static void printInputErrorMessage() {
        System.out.println("Некорректный ввод. Попробуйте ещё раз.");
    }

    public static void printReturnToMenuMessage() {
        System.out.println("Для повтора введите 1. Для возврата в предыдущее меню введите 0");

    }

    public static void consoleClener() {
        for (int q = 0; q < 100; q++) System.out.println();
    }


}