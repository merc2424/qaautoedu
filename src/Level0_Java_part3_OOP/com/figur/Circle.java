package Level0_Java_part3_OOP.com.figur;

/**
 * Created by dyakonov on 21.11.2016.
 */
public class Circle extends Figure {

    double radius;

    public Circle(double x, double y, double radius) {
        this.setX(x);
        this.setY(y);
        this.radius = radius;
        this.setType("Circle");
    }

    @Override
    public void movement(double x, double y) {
        super.movement(x, y);
    }

    @Override
    public int calculationArea() {
        return (int) (Math.PI * Math.pow(radius, 2));
    }

    @Override
    public void resizing(double r) {
        radius *= r;
    }

    @Override
    public String getType() {
        return "Circle";
    }


}

