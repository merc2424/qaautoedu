package Level0_Java_part3_OOP.com.figur;

/**
 * Created by dyakonov on 21.11.2016.
 */
public abstract class Figure {
    private double x;
    private double y;
    private String type;

    public void movement(double x, double y) {
        this.x += x;
        this.y += y;
    }

    abstract public int calculationArea();

    abstract public void resizing(double r);

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
}
