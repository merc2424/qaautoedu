package Level0_Java_part3_OOP.com.figur;

/**
 * Created by dyakonov on 21.11.2016.
 */
public class Square extends Figure {

    private double side;

    public Square(double x, double y, double side) {
        this.setX(x);
        this.setY(y);
        this.side = side;
        this.setType("Square");
    }

    @Override
   public void movement(double x, double y) {
        super.movement(x, y);
    }

    @Override
    public int calculationArea() {
        return (int)(side * side);
    }

    @Override
    public void resizing(double r) {
        side *= r;
    }

    @Override
    public String getType() {
        return "Square";
    }
}
