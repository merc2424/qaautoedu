package Level0_Java_part3_OOP.com.figur;

/**
 * Created by dyakonov on 21.11.2016.
 */
public class Triangle extends Figure {

    private double side;
    private double height;
    private String type = "Triangle";

    public Triangle(double x, double y, double side, double height) {
        this.setX(x);
        this.setY(y);
        this.side = side;
        this.height = height;
        this.setType("Triangle");
    }

    @Override
    public void movement(double x, double y) {
        super.movement(x, y);
    }

    @Override
    public int calculationArea() {
        return (int) ((side * height) / 2);
    }

    @Override
    public void resizing(double r) {
        side *= r;
        height *= r;
    }
}
