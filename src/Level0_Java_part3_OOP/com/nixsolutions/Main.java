package Level0_Java_part3_OOP.com.nixsolutions;

import Level0_Java_part3_OOP.com.figur.Circle;
import Level0_Java_part3_OOP.com.figur.Figure;
import Level0_Java_part3_OOP.com.figur.Square;
import Level0_Java_part3_OOP.com.figur.Triangle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * Created by dyakonov on 21.11.2016.
 */
public class Main {
    static Figure[] arrayFigures = new Figure[10];
    static BufferedReader buffer1 = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) {
        randomArrFigures();
        menu();
    }

    public static void menu() {
        try {
            consoleClener();
            printArray();
            printMenu();

            switch (Integer.parseInt(buffer1.readLine())) {
                case 1:
                    randomArrFigures();
                    break;
                case 2:
                    randomMovement();
                    break;
                case 3:
                    resizingFiguresOfArray();
                    break;
                case 4:
                    sortArraysOfFiguresAsc();
                    break;
                case 5:
                    sortArraysOfFiguresDsc();
                    break;
                case 0:
                    System.out.println("Всего доброго!");
                    System.exit(1);

                default: {
                    menu();
                }
            }
        } catch (IOException i) {
            menu();
        } catch (NumberFormatException n) {

            menu();
        }
        menu();
    }

    static void printMenu() {

        System.out.printf("%n==================================== Меню ======================================%n");
        System.out.println("|           Для навигации по меню используйте ввод цифр с клавиатуры:          |");
        System.out.printf("================================================================================%n");
        System.out.println("|  1 - Заполненить массив фигурами с произвольным размером и расположением     |");
        System.out.println("|  2 - Переместить все фигуры в массиве используя рандомный коэффициент        |");
        System.out.println("|  3 - Изменить размер всех фигур в массиве используя рандомный коэффициент    |");
        System.out.println("|  4 - Сортировка фирур в массиве по размеру .:|                               |");
        System.out.println("|  5 - Сортировка фирур в массиве по размеру |:.                               |");
        System.out.println("|  0 - Завершить работу программы                                              |");
        System.out.printf("================================================================================%n");
    }

    public static void sortArraysOfFiguresAsc() {

        for (int i = arrayFigures.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arrayFigures[j].calculationArea() > arrayFigures[j + 1].calculationArea()) {
                    Figure buferFigure = arrayFigures[j];
                    arrayFigures[j] = arrayFigures[j + 1];
                    arrayFigures[j + 1] = buferFigure;
                }
            }
        }
    }

    public static void sortArraysOfFiguresDsc() {

        for (int i = arrayFigures.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arrayFigures[j].calculationArea() < arrayFigures[j + 1].calculationArea()) {
                    Figure buferFigure = arrayFigures[j];
                    arrayFigures[j] = arrayFigures[j + 1];
                    arrayFigures[j + 1] = buferFigure;
                }
            }
        }
    }

    public static void resizingFiguresOfArray() {
        for (int i = 0; i < arrayFigures.length; i++) {
            arrayFigures[i].resizing(Math.random() * 2 + 0.1);
        }
    }

    public static void printArray() {
        System.out.println("============================== Array of Figures ================================");
        for (int i = 0; i < 10; i++) {
            System.out.printf("| Array[%1d]: Type -%9s | point.x = %5.1f | point.y = %5.1f | Area = %5d |%n", i, arrayFigures[i].getType(), arrayFigures[i].getX(), arrayFigures[i].getY(), arrayFigures[i].calculationArea());
            System.out.println("--------------------------------------------------------------------------------");
        }
    }

    public static void randomArrFigures() {
        for (int i = 0; i < arrayFigures.length; i++) {

            switch ((int) (Math.random() * 3) + 1) {
                case 1:
                    arrayFigures[i] = new Triangle(Math.random() * 20 - 10, Math.random() * 20 - 10, Math.random() * 50 + 1, Math.random() * 50 + 1);
                    break;

                case 2:
                    arrayFigures[i] = new Circle(Math.random() * 20 - 10, Math.random() * 20 - 10, Math.random() * 50 + 1);
                    break;

                case 3:
                    arrayFigures[i] = new Square(Math.random() * 20 - 10, Math.random() * 20 - 10, Math.random() * 50 + 1);
                    break;
            }
        }
    }

    public static void randomMovement() {
        for (int i = 0; i < arrayFigures.length; i++) {
            arrayFigures[i].movement(Math.random(), Math.random() * 10 - 5);
        }
    }

    public static void consoleClener() {
        for (int q = 0; q < 100; q++) System.out.println();
    }
}



